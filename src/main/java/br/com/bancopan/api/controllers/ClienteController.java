package br.com.bancopan.api.controllers;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bancopan.api.models.Cliente;
import br.com.bancopan.api.models.Endereco;
import br.com.bancopan.api.repository.ClienteRepository;
import br.com.bancopan.api.repository.EnderecoRepository;


@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	private ClienteRepository cliRepository;
	private EnderecoRepository endRepository;
	
	public ClienteController(ClienteRepository repo, EnderecoRepository endRep) {
		this.cliRepository = repo;
		this.endRepository = endRep;
		
		this.cliRepository.deleteAll();
		this.endRepository.deleteAll();
		
		Endereco endereco = new Endereco("09190620", "Rua Juazeiro", "AP 02", 437, "Paraiso", "Santo André", "SP");
		this.endRepository.insert(endereco);
		
		ArrayList<Cliente> entities = new ArrayList<Cliente>();
		Cliente cliente = new Cliente("Alexandre Martins", "30716214857", endereco);
		entities.add(cliente);
		this.cliRepository.insert(entities);
	}
	
	@GetMapping
	public List<Cliente> listAll() {
		return this.cliRepository.findAll();
	}
		
	@GetMapping(value="/{cpf}")
	public Cliente consultaPorCpf(@PathVariable("cpf") String cpf) {
		return this.cliRepository.findByCpf(cpf);
	}
}
