package br.com.bancopan.api.controllers;

import java.util.ArrayList;
import java.util.Comparator;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.bancopan.api.models.Endereco;
import br.com.bancopan.api.models.Municipio;
import br.com.bancopan.api.models.UF;

@RestController
@RequestMapping("/localidades")
public class EnderecoController {
	private String endpointIBGE = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/";
	private RestTemplate restTemplate;
	
	public EnderecoController() {
		restTemplate = new RestTemplate();
	}
	
	@GetMapping(value="/cep/{cep}")
	public Endereco consultaCep(@PathVariable("cep") String cep) {
//		TODO PESQUISAR NO MONGO, SE NÂO EXISTIR TRAZER O DA VIACEP
		
		String endpointViaCep = "https://viacep.com.br/ws/" + cep + "/json";
		Endereco endereco = restTemplate.getForObject(endpointViaCep, Endereco.class);
		
		return endereco;
	}
	
	@GetMapping(value="/estado/{id_estado}/municipios")
	public ArrayList<Municipio> consultaMunicipios(@PathVariable("id_estado") String id_estado) {
		ResponseEntity<ArrayList<Municipio>> response = restTemplate.exchange(
			endpointIBGE + id_estado + "/municipios",
			HttpMethod.GET,
			null,
			new ParameterizedTypeReference<ArrayList<Municipio>>() {}
		);
		
		ArrayList<Municipio> municipios = response.getBody();	
		return municipios;
				
	}
	
	@GetMapping(value="/estado")
	public ArrayList<UF> consultaEstado() {
		// TODO CONSULTAR ESTADOS. DEIXAR SP E RJ INICIAIS E DEPOIS ORDEM ALFABETICA
		ResponseEntity<ArrayList<UF>> response = restTemplate.exchange(
			endpointIBGE,
			HttpMethod.GET,
			null,
			new ParameterizedTypeReference<ArrayList<UF>>() {}
		);
			
		ArrayList<UF> estados = response.getBody();
		estados.sort(new Comparator<Object>() {

			@Override
			public int compare(Object o1, Object o2) {
				UF uf1 = (UF) o1;
				UF uf2 = (UF) o2;
				
				return uf1.getNome().compareToIgnoreCase(uf2.getNome());
			}
			
		});
		return estados;
		
	}
	
	public void altualizaEndereco() {
		// TODO ATUALIZAR ENDERECO DE UM CLIENTE X
	}
}
