package br.com.bancopan.api.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import br.com.bancopan.api.controllers.ClienteController;
import br.com.bancopan.api.repository.ClienteRepository;

@SpringBootApplication
@ComponentScan(basePackageClasses={ClienteController.class})
@EnableMongoRepositories(basePackageClasses={ClienteRepository.class})
public class APIApplication {

	public static void main(String[] args) {
		SpringApplication.run(APIApplication.class, args);
	}

}
