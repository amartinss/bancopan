package br.com.bancopan.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.bancopan.api.models.Endereco;

@Repository
public interface EnderecoRepository extends MongoRepository<Endereco, String> {
	
	
	
}
