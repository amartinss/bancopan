package br.com.bancopan.api.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.bancopan.api.models.Cliente;

@Repository
public interface ClienteRepository extends MongoRepository<Cliente, String> {
	Cliente findById(ObjectId id);

	Cliente findByCpf(String cpf);

	

}
